<?php
/**
 * French language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Jonas Fourquier <jonas@tuxfamily.org>
 */

$lang['pageFor']                = 'Cette page concerne';
$lang['pageForOther']           = 'Voir cette page pour : ';
