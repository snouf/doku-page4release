<?php
/**
 * English language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Jonas Fourquier <jonas@tuxfamily.org>
 */

$lang['pageFor']                = 'Page for';
$lang['pageForOther']           = 'See this page for: ';
