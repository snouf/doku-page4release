<?php
/**
 * page4relesae index rebuilder
 * authors Jonas Fourquier <jonas@mythtv-fr.org>
 */


if (!defined('DOKU_INC'))
    define('DOKU_INC', realpath(dirname(__FILE__) . '/../../../') . '/');

if (!defined('NL'))
    define('NL', "\n");

require_once(DOKU_INC.'inc/init.php');
require_once(DOKU_INC.'inc/common.php');
require_once(DOKU_INC.'inc/pageutils.php');
require_once(DOKU_INC.'inc/auth.php');
require_once(DOKU_INC.'inc/search.php');
require_once(DOKU_INC.'inc/indexer.php');

//close session
session_write_close();

header('Content-Type: text/plain; charset=utf-8');

if (!auth_isadmin()) {
    die('for admins only');
}

$pages = array();
search($pages, $conf['datadir'], 'search_allpages', array());

echo "\n====================================================\nScan des pages\n====================================================\n\n";
$page4release = array();
foreach ($pages as $page) {
    echo '[['.$page['id'].']]';
    $filename = array_pop(explode(':',$page['id']));
    $release = p_get_metadata($page['id'], 'page4release',True);
    if ($release) {
        echo "\t$filename {{pour $release}}\n";
        $page4release[$filename][$page['id']] = $release;
    } else {
        echo "\n";
    }
}
echo "\n====================================================\nIndex des pages\n====================================================\n\n";
$pagesExpired = array();
foreach ($page4release as $filename => $pages) {
    $pagesID = array_keys($pages);
    echo $filename.': [['.join(']], [[',$pagesID)."]]\n";
    $pagesExpired = array_merge($pagesExpired,$pagesID);
}
io_saveFile($conf['indexdir'].'/page4release.idx', serialize($page4release));
echo "\n====================================================\nCache expiré\n====================================================\n\n";
foreach ($pagesExpired as $pageExpired) {
    p_set_metadata($pageExpired, array('cache' => 'expire'), False, False);
    echo $pageExpired."\n";
}
?>
