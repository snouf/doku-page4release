<?php
/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Jonas Fourquier <jonas@tuxfamily.org>
 */

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');


class syntax_plugin_page4release extends DokuWiki_Syntax_Plugin {

    function getType(){
        return 'substition';
    }
    function getPType() {
        return 'block';
    }

    function getInfo(){
        return confToHash(dirname(__FILE__).'/info.txt');
    }

    function getSort(){
        return 305;
    }

    function connectTo($mode) {
        global $ID;
        p_set_metadata($ID, array('page4release'=>False), False, True);
        $this->Lexer->addSpecialPattern('\{\{for .*?\}\}',$mode,'plugin_page4release');
        $this->Lexer->addSpecialPattern('\{\{pour .*?\}\}',$mode,'plugin_page4release');
    }

    function handle($match, $state, $pos, &$handler) {
        if (substr($match,2,3) == 'for')
            return trim(substr($match,6,-2));
        elseif (substr($match,2,4) == 'pour')
            return trim(substr($match,6,-2));
    }

    function render($mode, &$renderer, $data) {
        global $conf,$ACT, $ID;
        if($mode == 'xhtml') {
            $INFO = pageinfo();
            $release = $data;
            $linkOtherReleases = array();
            $page4release = unserialize(io_readFile($conf['indexdir'].'/page4release.idx', false));
            $filename = array_pop(explode(':',$ID));
            if (isset($page4release[$filename])) {
                foreach ($page4release[$filename] as $otherId=>$otherRelease) {
                    if ($otherId != $ID)
                        $linkOtherReleases[] = '<a class="wikilink1" href="'.wl($otherId).'">'.$otherRelease.'</a>';
                }
            }
            $doc = '<div class="page4release">';
            if ($release)
                $doc .= $this->getLang('pageFor').' <span class="active_page4release">'.$release.'</span>. ';
            if (!empty($linkOtherReleases))
                $doc .= $this->getLang('pageForOther').join(', ',$linkOtherReleases);
            $doc .= '</div>';
            $renderer->doc .= $doc;
            return true;
        } elseif ($mode == 'metadata' && $ACT != 'preview' && !$REV) {
            p_set_metadata($ID, array('page4release'=>$data), False, True);
            return true;
        }
        return false;
    }
}
?>
