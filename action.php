<?php
/**
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Jonas Fourquier <jonas@tuxfamily.org>
 */

if(!defined('DOKU_INC')) die();

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'action.php');

class action_plugin_page4release extends DokuWiki_Action_Plugin {

    function getInfo() {
        return confToHash(dirname(__FILE__).'/info.txt');
    }

    function register(&$contr) {
        $contr->register_hook('TPL_METAHEADER_OUTPUT', 'BEFORE', $this, 'indexer', array());
    }

    function indexer(&$event, $param) {
        global $conf, $ACT, $ID;
        if ($ACT != 'preview' && !$REV) {
            $page4release = unserialize(io_readFile($conf['indexdir'].'/page4release.idx', false));
            $filename = array_pop(explode(':',$ID));
            $release = p_get_metadata($ID, 'page4release',True);
            if ((is_array($page4release)) && (array_key_exists($filename, $page4release))) {
                // Le nom de page se trouve déjà dans l'index
                if (    (array_key_exists($ID,$page4release[$filename]) && ($page4release[$filename][$ID] != $release))
                     || (!array_key_exists($ID,$page4release[$filename]) && ($release))
                   ) {
                    // La page qu'on modifie se trouve déjà dans l'index et release à été modifié.
                    // OU
                    // La page qu'on modifie ne se trouve pas dans l'index et une release est spécifié.
                    // Parcours l'ensemble des ID avec le même filename pour purger leur cache.
                    foreach ($page4release[$filename] as $otherId=>$otherRelease) {
                        if ($otherId != $ID)
                            p_set_metadata($otherId, array('cache' => 'expire'), False, False);
                    }
                }
                if ($release)
                    // contient une chaine, ajout / met a jour l'index
                    $page4release[$filename][$ID] = $release;
                else
                    // ne contient plus de chaine, y supprime de l'index
                    unset($page4release[$filename][$ID]);
            } elseif ($release) {
                // Ne se trouve pas encore dans l'index mais contient une chaine, y ajoute
                $page4release[$filename] = array($ID => $release);
            }
            io_saveFile($conf['indexdir'].'/page4release.idx', serialize($page4release));
        }
    }
}
